
import os
import subprocess
class cd:
    """Context manager for changing the current working directory"""
    def __init__(self, newPath):
        self.newPath = os.path.expanduser(newPath)

    def __enter__(self):
        self.savedPath = os.getcwd()
        os.chdir(self.newPath)

    def __exit__(self, etype, value, traceback):
        os.chdir(self.savedPath)

with cd("~/test/sample_app"):
    with open(os.devnull, 'w') as DEVNULL:
        subprocess.Popen(
            ["git","pull"],
            stdout=DEVNULL,
            stderr=DEVNULL,
         )
